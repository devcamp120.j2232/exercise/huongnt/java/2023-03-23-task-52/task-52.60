import java.util.ArrayList;
public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<User> arrayList = new ArrayList<>();
        //khởi tạo User với các tham số khác nhau
        User user0 = new User();
        User user2 = new User("huong", "huongnguyen", true);    
                //thêm objcect order vào danh sách
                arrayList.add(user0);
                arrayList.add(user2);
            //in ra màn hình
            for (User user:arrayList){
                System.out.println(user.toString());
            }
    
    }
}
