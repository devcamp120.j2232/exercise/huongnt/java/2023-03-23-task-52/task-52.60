public class User {
    String password;
    String Username;
    boolean Enabled;

    //khỏi tạo user
    public User() {
    }
    
    public User(String password, String username, boolean enabled) {
        this.password = password;
        Username = username;
        Enabled = enabled;
    }


    //getter & setter
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getUsername() {
        return Username;
    }
    public void setUsername(String username) {
        Username = username;
    }
    public boolean isEnabled() {
        return Enabled;
    }
    public void setEnabled(boolean enabled) {
        Enabled = enabled;
    }

    @Override
    public String toString(){
     return "User [password = " + password + ", username = " + Username + ", Enabled = " + Enabled + "]";
    }


}
